def quel_est_sa_place():
    """
    Cette fonction demande à l'utilisateur de saisir une phrase et un caractère,
    puis recherche la position de ce caractère dans la phrase. Elle affiche ensuite
    les positions où le caractère a été trouvé ou un message indiquant que le caractère
    n'est pas présent dans la phrase.

    Entrées:
    - Aucun argument n'est pris en compte lors de l'appel de la fonction. L'utilisateur
      est invité à saisir une phrase et un caractère à l'aide des fonctions 'input'.

    Sortie:
    - Cette fonction n'a pas de valeur de retour explicite. Elle affiche les résultats à l'écran
      en fonction des résultats de la recherche.

    Exemple d'utilisation:
    >>> quel_est_sa_place()
    Veuillez saisir une phrase : Bonjour tout le monde
    Veuillez saisir un caractère : o

    On retrouve le caractère 'o' à la position [2, 4, 9, 13]

    >>> quel_est_sa_place()
    Veuillez saisir une phrase : Hello, World!
    Veuillez saisir un caractère : x

    Le caractère 'x' n'est pas présent dans la phrase
    """
    # Demande à l'utilisateur de saisir une phrase
    reponse_phrase = input("Veuillez saisir une phrase : ")

    # Demande à l'utilisateur de saisir un caractère
    reponse_caractere = input("Veuillez saisir un caractère : ")

    # Initialise une liste pour stocker les positions du caractère et changement du booléen selon qu'il est trouvé ou non
    positions = []
    inclu = False

    for recherche in range(len(reponse_phrase)):
        if reponse_phrase[recherche] == reponse_caractere:
            positions.append(recherche + 1)
            inclu = True

    # On affiche la réponse
    if inclu:
        print(f"\nOn retrouve le caractère '{reponse_caractere}' à la position {positions}")
    else:
        print(f"Le caractère '{reponse_caractere}' n'est pas présent dans la phrase")

# Appel de la fonction pour illustrer son utilisation
# quel_est_sa_place()