.. Quelle est sa place documentation master file, created by
   sphinx-quickstart on Mon Oct  9 10:22:29 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Quelle est sa place's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
